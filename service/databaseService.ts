import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { News } from 'src/models/news';
import { Forum } from 'src/models/forum';
import { ProfileData } from 'src/models/profileData';

@Injectable()
export class DatabaseService{
    idDb;
    newsDb;
    forumDb;
    id;

    list:News[] = [];
    forum:Forum[]=[];
    db:AngularFireDatabase;

    constructor(private _db:AngularFireDatabase){
        this.db = _db;
        this.idDb = this.db.list('/user').valueChanges();
        this.newsDb = this.db.list('/list').valueChanges();
        this.forumDb = this.db.list('/forum').valueChanges();
    }
    getNews(id:string):News[]{
        this.list = [];
        this.newsDb.subscribe((result: News[])=>{
            result.forEach((item)=>{
                this.list.push(item);
            })
        })
        return this.list;
    }
    getAllNews():News[]{
        this.list = [];
        this.newsDb.subscribe((result: News[])=>{
            result.forEach((item)=>{
                this.list.push(item);
            })
        })
        return this.list;
    }
    getAllForum():Forum[]{
        this.forum = [];
        this.forumDb.subscribe((result: Forum[])=>{
            result.forEach((item)=>{
                this.forum.push(item);
            })
        })
        return this.forum;
    }
    addNews(title:string,id:string,content:string,date:string){
        const newNews = this.db.list('/news').push({
            title:title,
            id:id,
            content:content,
            date:date
        })
    }
    addCommentForum(userId:string,forumId:string,content:string){
        this.db.list('forumComment').push({
            userId:userId,
            forumId:forumId,
            content:content
        })
    }
    addCommentNews(userId:string,newsId:string,content:string){
        this.db.list('newsComment').push({
            userId:userId,
            newsId:newsId,
            content:content
        })
    }
    addForum(title:string,id:string,content:string,ts:string,date:string){
        const newForum = this.db.list('/forum').push({
            title:title,
            id:id,
            content:content,
            ts:ts,
            date:date
        });
    }
    getUser(email:string){


    }
    editUser(data:any,edited:any){
        console.log(data);
        this.db.object("/user/"+data).update({email:edited.email,password:edited.password,username:edited.username,phonenumber:edited.phonenumber});
        
    }
    addUser(username:string, email:string, phonenumber:number, password:string){
         const newId = this.db.list('/user').push({
            email:email,
            password:password,
            phonenumber:phonenumber,
            username:username
        }); 
    }
}