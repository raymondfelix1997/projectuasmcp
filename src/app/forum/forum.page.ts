import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/service/databaseService';
import { AngularFireDatabase } from 'angularfire2/database';
import { News } from 'src/models/news';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { Forum } from 'src/models/forum';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.page.html',
  styleUrls: ['./forum.page.scss'],
})
export class ForumPage implements OnInit {
  lstOfForum:Forum[] =[];

  constructor(private db:DatabaseService,private navCtrl:NavController) {
    this.lstOfForum = this.db.getAllForum();
    console.log(this.lstOfForum);
  }

  reroute(id:string){
    let fullpath = "/news-detail/" + id;
    this.navCtrl.navigateForward(fullpath);
  }

  ngOnInit() {
  }

}
