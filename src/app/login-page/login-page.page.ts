import { Component, OnInit, NgModule } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthInfo } from 'src/models/authInfo';
import { SignupPagePage } from '../signup-page/signup-page.page';
import * as localforage from "localforage";
import { SMS } from '@ionic-native/sms/ngx';

@NgModule()
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {
  ngForm: FormGroup;
  email: string;
  password: string;
  id;
  generatedString:string;

  ngOnInit(){

  }

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private db:AngularFireDatabase,
    private sms:SMS) 
  {
    this.GenerateString();
    this.id = db.list('/user').valueChanges();
    this.id.forEach(element => {
      console.log(element);
    });
    localforage.getItem("loggedInId").then(function(val){
      if(val!= null){
        navCtrl.navigateRoot('/home');
      }
    }).catch(function(msg){
      console.log("no data");
      console.log(msg);
    })
  }

  // LoginPage(){
  //   console.log("Username: "+ this.username);
  //   console.log("Password: "+ this.password);
  // }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  goToSignupForm() {
    console.log("SignupFormPage");
    this.navCtrl.navigateForward('/signup-page');
  }

  async goToLanding(f) {
    let movepageLoadingController = await this.loadingCtrl.create({});
    await movepageLoadingController.present().then(()=>{
      let exist = false;
      this.id.forEach(arr => {
        arr.forEach(item => {
          if(item.email == f.email){
            if(item.password == f.password){
              exist = true;
              this.generatedString = this.GenerateString();
              this.sms.send(item.phonenumber,this.generatedString);
              this.presentToast();
              console.log(item.phonenumber);
              this.navCtrl.navigateForward("otp-verification-check/" + this.generatedString + "/" + f.email);
            }
          }
        });
      });
      if(!exist){
        this.ErrorSigninToast();
        movepageLoadingController.dismiss();
      }
    });
    
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Berhasil Login!',
      duration: 3000,
      position: 'bottom'
    }).then((toastData)=>{
      toastData.present();
    });
  }

  GenerateString():string{
    return Math.random().toString(36).substring(2, 6);
  }

  ErrorSigninToast() {
    let toast = this.toastCtrl.create({
      message: 'Username atau Password Salah ',
      duration: 3000,
      position: 'bottom'
    }).then((toastData)=>{
      toastData.present();
    });;
  }

}
