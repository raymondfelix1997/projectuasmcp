import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/service/databaseService';
import { AngularFireDatabase } from 'angularfire2/database';
import { News } from 'src/models/news';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import * as localforage from "localforage";

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  news:News[] =[];

  constructor(private db:DatabaseService,private navCtrl:NavController) {
    this.news = db.getAllNews();
 }

  reroute(id:string){
    let fullpath = "/news-detail/" + id + "/";
    localforage.getItem("loggedInId").then((val)=>{
      this.navCtrl.navigateForward(fullpath+val);
    });
  }

  ngOnInit() {
  }

}
