import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { News } from 'src/models/news';
import { DatabaseService } from 'src/service/databaseService';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { NewsComment } from 'src/models/newsComment';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
})
export class NewsDetailPage implements OnInit {
  content:News;
  commentBox:Boolean;
  commentContent:any;
  dataId:any;
  dataUser:any;
  comments:NewsComment[] = [];

  constructor(private route: ActivatedRoute, private db:DatabaseService,private dbFire:AngularFireDatabase) {
    this.commentBox = false;
    this.content = {title:"",date:"",content:"",id:""};   
    this.dataId = this.route.snapshot.paramMap.get('id');
    this.dataUser = this.route.snapshot.paramMap.get('userId');

    this.dbFire.object('/list').query.once("value").then(data=>{
      data.forEach(element => {
        if (this.dataId == element.val().id){
          this.content = element.val();
        }
      });
    });

    this.dbFire.list("/newsComment").valueChanges().subscribe((data:NewsComment[])=>{
      this.comments = [];
      data.forEach(e => {
        if(e.newsId == this.dataId){
          this.comments.push(e);
        }
      });
    });

    

    
  }
  executeComment(){
    this.db.addCommentNews(this.dataUser,this.dataId,this.commentContent)
  }
  ngOnInit() {

  }

}
