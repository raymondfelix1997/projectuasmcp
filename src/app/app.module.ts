import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { DatabaseService } from 'src/service/databaseService';

import{FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SMS } from '@ionic-native/sms/ngx';

export const fireConfig = {

  apiKey: "AIzaSyDYb2fNhUpfNtLVn2YcbVWw0UZDaiS7FqU",
  authDomain: "mobile-19431.firebaseapp.com",
  databaseURL: "https://mobile-19431.firebaseio.com",
  projectId: "mobile-19431",
  storageBucket: "mobile-19431.appspot.com",
  messagingSenderId: "1083037318732",
  appId: "1:1083037318732:web:0408099870268dad"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, 
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(fireConfig),
    AngularFireDatabaseModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    DatabaseService,
    SMS
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
