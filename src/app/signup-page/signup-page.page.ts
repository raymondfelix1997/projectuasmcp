import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatabaseService } from 'src/service/databaseService';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.page.html',
  styleUrls: ['./signup-page.page.scss'],
})
export class SignupPagePage implements OnInit {
  user: {nama: string, email: string, phonenumber: number};
  
  OnInit(){

  }
  constructor(private toastCtrl: ToastController,private db:DatabaseService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupFormPage');
  }

  userForm: FormGroup;

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.userForm = new FormGroup({
      username: new FormControl (null, Validators.required),
      password: new FormControl (null, Validators.required),
      confirmpassword: new FormControl (null, Validators.required),
      email: new FormControl (null, Validators.required),
      phonenumber: new FormControl (null, Validators.required)
    })
  }

  onSubmit(){
    console.log(this.userForm.value)
    // this.navCtrl.push(SignupFormPage);
    const password = this.userForm.value.password;
    // console.log(password);
    const password2 = this.userForm.value.confirmpassword;
    // console.log(password2);
    if (password !== password2 ) {
      this.confirmpassToast();
    } else 
    {
      this.db.addUser(this.userForm.value.username,this.userForm.value.email,this.userForm.value.phonenumber,this.userForm.value.password);
    }
  }
  email(email: any, password: any): any {
    throw new Error("Method not implemented.");
  }
 
  signupToast(err) {
    let toast = this.toastCtrl.create({
      message: err.message,
      duration: 3000,
      position: 'top'
    });
  }

  confirmpassToast(){
    let toast = this.toastCtrl.create({
      message: 'password gak sama',
      duration: 3000,
      position: 'top'
    });
  }


}
