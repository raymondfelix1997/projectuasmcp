import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OtpVerificationCheckPage } from './otp-verification-check.page';

const routes: Routes = [
  {
    path: '',
    component: OtpVerificationCheckPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OtpVerificationCheckPage]
})
export class OtpVerificationCheckPageModule {}
