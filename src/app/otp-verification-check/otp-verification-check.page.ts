import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as localforage from "localforage";
import { NavController, ToastController } from '@ionic/angular';
@Component({
  selector: 'app-otp-verification-check',
  templateUrl: './otp-verification-check.page.html',
  styleUrls: ['./otp-verification-check.page.scss'],
})
export class OtpVerificationCheckPage implements OnInit {
  otp:String;
  userInput:String;
  userEmail:String;

  constructor(private route: ActivatedRoute,private navCtrl:NavController,private toastCtrl:ToastController) { 
    this.otp = this.route.snapshot.paramMap.get('genString');
    this.userEmail = this.route.snapshot.paramMap.get('email');
  }

  checkOTP(){
    if(this.userInput == this.otp){
      localforage.setItem('loggedInId',this.userEmail);
      this.navCtrl.navigateRoot('/home');
    }else{
      let toast = this.toastCtrl.create({
        message: 'OTP salah!',
        duration: 3000,
        position: 'bottom'
      }).then((toastData)=>{
        toastData.present();
      });
    }
  }

  ngOnInit() {
  }

}
