import { Component, OnInit } from '@angular/core';
import * as localforage from "localforage";
import { DatabaseService } from 'src/service/databaseService';
import { AngularFireDatabase } from 'angularfire2/database';
import { ProfileData } from 'src/models/profileData';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  profiles:ProfileData;
  fbData:any;
  x:string;

  constructor(public toastCtrl:ToastController,public navCtrl:NavController,private route: ActivatedRoute, private db:DatabaseService,private dbFire:AngularFireDatabase) {
    this.profiles={username:"",password:"",email:"",phonenumber:0};
    let dataId = this.route.snapshot.paramMap.get('email');
    if(dataId == "take"){
      localforage.getItem("loggedInId").then((val)=>{
        navCtrl.navigateRoot('/home/profile/'+val);
      });
      console.log(dataId);
    } else{
      this.getData(dataId);
    } //localforage gajelas 
  }

  edit(){
    this.db.editUser(this.fbData,this.profiles);
    this.presentToast();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Profil berhasil di ubah',
      duration: 3000,
      position: 'bottom'
    }).then((toastData)=>{
      toastData.present();
    });
  }

  getData(val:string){
    this.dbFire.object('/user').query.once("value").then(data=>{
     // this.fbData = data;
      data.forEach(element=>{
          if(val as string == element.val().email){
            this.fbData = element.key;
              this.profiles = element.val();
          }
      })
    })
  }

  ngOnInit() {

  }

}
