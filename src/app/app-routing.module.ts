import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login-page', pathMatch: 'full' },
  { path: 'login-page', loadChildren: './login-page/login-page.module#LoginPagePageModule' },
  { path: 'signup-page', loadChildren: './signup-page/signup-page.module#SignupPagePageModule' },
  { path: 'home', loadChildren:'./home/home.module#HomePageModule'},
  { path: 'news-detail/:id', loadChildren: './news-detail/news-detail.module#NewsDetailPageModule' },
  { path: 'news-detail/:id/:userId', loadChildren: './news-detail/news-detail.module#NewsDetailPageModule' },
  { path: 'user-profile', loadChildren: './user-profile/user-profile.module#UserProfilePageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
  { path: 'add-comment-modal', loadChildren: './add-comment-modal/add-comment-modal.module#AddCommentModalPageModule' },
  { path: 'forum-detail/:id', loadChildren: './forum-detail/forum-detail.module#ForumDetailPageModule' },
  { path: 'forum-detail/:id/:userId', loadChildren: './forum-detail/forum-detail.module#ForumDetailPageModule' },
  { path: 'otp-verification-check/:genString/:email', loadChildren: './otp-verification-check/otp-verification-check.module#OtpVerificationCheckPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
