import { Component, OnInit } from '@angular/core';
import { Forum } from 'src/models/forum';
import { ActivatedRoute } from '@angular/router';
import { DatabaseService } from 'src/service/databaseService';
import { AngularFireDatabase } from 'angularfire2/database';
import { ForumComment } from 'src/models/forumComment';

@Component({
  selector: 'app-forum-detail',
  templateUrl: './forum-detail.page.html',
  styleUrls: ['./forum-detail.page.scss'],
})
export class ForumDetailPage implements OnInit {
  content:Forum;
  commentBox:Boolean;
  commentContent:any;
  dataId:any;
  dataUser:any;
  comments:ForumComment[] = [];
  constructor(private route: ActivatedRoute, private db:DatabaseService,private dbFire:AngularFireDatabase) { 
    this.commentBox = false;
    this.content = {title:"",date:"",content:"",id:""};   
    this.dataId = this.route.snapshot.paramMap.get('id');
    this.dataUser = this.route.snapshot.paramMap.get('userId');
    console.log(this.dataId);

    this.dbFire.object('/forum').query.once("value").then(data=>{
      data.forEach(element => {
        console.log(element.val());
        if (this.dataId == element.val().id){
          this.content = element.val();
          
        }
      });
    });

    this.dbFire.list("/forumComment").valueChanges().subscribe((data:ForumComment[])=>{
      this.comments = [];
      data.forEach(e => {
        if(e.forumId == this.dataId){
          this.comments.push(e);
        }
      });
    });

  }

  executeComment(){
    this.db.addCommentForum(this.dataUser,this.dataId,this.commentContent)
  }

  ngOnInit() {
  }

}
