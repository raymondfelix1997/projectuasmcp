import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumDetailPage } from './forum-detail.page';

describe('ForumDetailPage', () => {
  let component: ForumDetailPage;
  let fixture: ComponentFixture<ForumDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
