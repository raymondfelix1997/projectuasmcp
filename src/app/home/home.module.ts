import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { HomePage } from './home.page';

const routes:Routes = [
  {
    path: '',
    redirectTo: '/home/news',
    pathMatch: 'full'
  },
  { 
    path: '',
    component:HomePage,
    children:[
      { path:'news', loadChildren:'../news/news.module#NewsPageModule' },
      { path:'forum', loadChildren:'../forum/forum.module#ForumPageModule'},
      { path:'profile/:email', loadChildren:'../user-profile/user-profile.module#UserProfilePageModule'},
      { path :'logout',loadChildren:'../logout/logout.module#LogoutPageModule'}
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
