import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  pages = [
    {
      title:'List Lapangan',
      url:'/home/news',
      icon:'football'
    },{
      title:'Booking',
      url:'/home/forum',
      icon:'add'
    },{
      title:'Profile',
      url:'/home/profile/take',
      icon:'person'
    },{
      title:'Logout',
      url:'/home/logout',
      icon:'exit'
    }
  ]
 
  constructor() { }
}
