import { Component, OnInit } from '@angular/core';
import * as localforage from "localforage";
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private navCtrl:NavController) { 
    localforage.clear();
    navCtrl.navigateRoot('');
  }

  ngOnInit() {
  }

}
